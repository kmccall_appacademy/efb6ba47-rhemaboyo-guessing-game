# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = (100 * rand).ceil
  puts 'guess a number'
  input = gets.chomp.to_i
  guesses = 1
  while input != number
    puts "#{input} is too high" if input > number
    puts "#{input} is too low" if input < number
    raise NoMoreInput if input > 100 || input < 1
    puts "Try again!"
    input = gets.chomp.to_i
    guesses += 1
  end
  puts "The number is #{number}! You win! Number of tries: #{guesses}"
end

def file_shuffler
  puts 'Input filename, please.'
  user_file = gets.chomp
  contents = File.readlines(user_file).shuffle
  new_file = File.new("{user_file}-shuffled.txt")
  File.open(new_file, 'w') { |f| f.puts contents }
end
